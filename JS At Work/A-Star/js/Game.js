var canvas;
var canvasContext;

const WALK_SPEED = 5;
const FPS = 144;
const LOCATE_MOUSE = false;

var searching = false;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('This is some sort of loading screen', canvas.width/10, canvas.height/3, 'blue', 80);
	
	startGame();
	setInterval(function() {fpsLoop();}, 1000/FPS);
}

function startGame() {
	gameReset();
	registerEvents();
}

function gameReset() {
	var levelInitialized = initLevel();
	
	while(!levelInitialized) {
		console.log("Initializing level");
	}
	
	initSim();
	searching = true;
	drawLevel();
}

function fpsLoop() {
	if(searching) {
		getCurent();
	
		if(currentNode == goalNode || currentNode == null) {
			console.log("Found it!");
			searching = false;
		}
		currentNode.getNeighbours();
		
		for(var neighbour of currentNode.neighbours) {
			if(!neighbour.isOpen || neighbour.g > (currentNode.g+STEP_VALUE)) { // New node
				neighbour.g = (currentNode.g+neighbour.stepValue);
				neighbour.setValuesHandF();
				neighbour.parentTile = currentNode;
				
				if(!neighbour.isOpen) {
					neighbour.isOpen = true;
					openSet.push(neighbour);
				}
			}
		}
		// Level
		drawCurrent();
	}
	
	findMouseTile();
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		colorText(mouseTileX+","+mouseTileY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
	
	//for(var i=0; i < openSet.length; i++) {
	//	var node = openSet[i];
	//	colorText(node.g, node.col*tileSize+5, node.row*tileSize+20, 'yellow', 15);
	//	colorText(node.h, node.col*tileSize+5, node.row*tileSize+40, 'yellow', 15);
	//}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

