// Class tile
class cTile {
	constructor(col, row, index, type) {
		this.col = col;
		this.row = row;
		this.index = index;
		this.tileType = type;
		this.neighbours = []
		
		// Default values
		this.parentTile = undefined;
		this.stepValue = 10;
		this.isOpen = false;
		this.isClosed = false;
		this.g = 0;
		this.h = 0;
		this.f = 0;
	}
	
	setValH() {
		this.h = this.getRemaining();
	}
	
	setValF() {
		this.f = this.g + this.h;
	}
	
	setValuesHandF() {
		this.setValH();
		this.setValF();
	}
	
	getRemaining() {
		var moves = 0;
		
		moves += Math.abs(tileMap.get(goalTileIndex).col - this.col);
		moves += Math.abs(tileMap.get(goalTileIndex).row - this.row);
		
		var pytA = Math.abs(tileMap.get(goalTileIndex).col - this.col);
		var pytB = Math.abs(tileMap.get(goalTileIndex).row - this.row);
		
		var pytC = Math.sqrt(pytA*pytA + pytB*pytB);
		
		return (pytC*10);
	}
	
	getNeighbours() {
		// Straight
		var left = tileMap.get(this.index-1);
		if(this.validateNeighbour(left)) {
			if(left.row == this.row) {
				this.neighbours.push(left);
			}
		}
		
		var right = tileMap.get(this.index+1);
		if(this.validateNeighbour(right)) {
			if(right.row == this.row) {
				this.neighbours.push(right);
			}
		}
		
		var above = tileMap.get(this.index-LEVEL_COLS);
		if(this.validateNeighbour(above)) {
			if(above.col == this.col) {
				this.neighbours.push(above);
			}
		}
		
		var below = tileMap.get(this.index+LEVEL_COLS);
		if(this.validateNeighbour(below)) {
			if(below.col == this.col) {
				this.neighbours.push(below);
			}
		}
		
		var armpit = false;
		
		// Diagonal
		var leftTop = tileMap.get(this.index-LEVEL_COLS-1);
		armpit = this.checkArmpit(left, above);
		if(this.validateNeighbour(leftTop) && !armpit) {
			if(leftTop.row == this.row-1 && leftTop.col == this.col-1) {
				this.neighbours.push(leftTop);
				leftTop.stepValue = 14;
			}
		}
		
		var rightTop = tileMap.get(this.index-LEVEL_COLS+1);
		armpit = this.checkArmpit(right, above);
		if(this.validateNeighbour(rightTop) && !armpit) {
			if(rightTop.row == this.row-1 && rightTop.col == this.col+1) {
				this.neighbours.push(rightTop);
				rightTop.stepValue = 14;
			}
		}
		
		var leftBotom = tileMap.get(this.index+LEVEL_COLS-1);
		armpit = this.checkArmpit(left, below);
		if(this.validateNeighbour(leftBotom) && !armpit) {
			if(leftBotom.col == this.col-1 && leftBotom.row == this.row+1) {
				this.neighbours.push(leftBotom);
				leftBotom.stepValue = 14;
			}
		}
		
		var rightBotom = tileMap.get(this.index+LEVEL_COLS+1);
		armpit = this.checkArmpit(right, below);
		if(this.validateNeighbour(rightBotom) && !armpit) {
			if(rightBotom.col == this.col+1 && rightBotom.row == this.row+1) {
				this.neighbours.push(rightBotom);
				rightBotom.stepValue = 14;
			}
		}
	}
	
	validateNeighbour(node) {
		if(node && node.tileType != LEVEL_WALL && !node.isClosed) {
			return true;
		} else {
			return false;
		}
	}
	
	checkArmpit(node1, node2) {
		  if(node1.tileType == LEVEL_WALL && node2.tileType == LEVEL_WALL) {
			  return true;
		  } else {
			  return false;
		  }
	}
	
	draw(color) {
		var x = this.col*tileSize;
		var y = this.row*tileSize;
		
		colorRect(x, y, tileSize, tileSize, color)
	}
}

/*
G: Score from start to this node. Increse by 10 for horizontal/vertical moves, increse 14 for diagonal (If included)
H: Score for remaining distanceance in straight line (Ignoring obstacles) Go diagonal until you hit row/column of target, then go straight
F: G + H. This is the score we look at to know which note to check next. Always check lowest. If equal, check lowest H
*/