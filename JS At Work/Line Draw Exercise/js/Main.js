const CANVAS_COLOR = 'blue'
const CANVAS_WIDTH = 1600;
const CANVAS_HEIGHT = 800;
const FPS = 100;

const START = [100,100];
const ANG = 45;
const DIST = 200;

var scripts = [];
var scriptsToLoad = 0;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, 'white', false);
	
	// Common Load Scripts
	loadScripts(scripts, "js/");
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = "Line them Up!";
}