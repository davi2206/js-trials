// Class tile
class cTile {
	constructor(col, row, index, type) {
		this.col = col;
		this.row = row;
		this.index = index;
		this.tileType = type;
		this.neighbours = []
		
		// Default values
		this.parentTile = undefined;
		this.stepValue = 10;
		this.isOpen = false;
		this.isClosed = false;
		this.walls = 0;
	}
	
	getNeighbours() {
		// Straight
		var left = tileMap.get(this.index-1);
		if(this.validateNeighbour(left)) {
			if(left.row == this.row) {
				this.neighbours.push(left);
			}
		}
		
		var right = tileMap.get(this.index+1);
		if(this.validateNeighbour(right)) {
			if(right.row == this.row) {
				this.neighbours.push(right);
			}
		}
		
		var above = tileMap.get(this.index-LEVEL_COLS);
		if(this.validateNeighbour(above)) {
			if(above.col == this.col) {
				this.neighbours.push(above);
			}
		}
		
		var below = tileMap.get(this.index+LEVEL_COLS);
		if(this.validateNeighbour(below)) {
			if(below.col == this.col) {
				this.neighbours.push(below);
			}
		}
		
		return this.walls;
	}
	
	validateNeighbour(node) {			
		if(node && (node.tileType == LEVEL_UNDEFINED || node.tileType == LEVEL_WALL) && !node.isOpen) {
			this.walls++;
			return true;
		} else {
			return false;
		}
	}
	
	checkArmpit(node1, node2) {
		  if(node1.tileType == LEVEL_WALL && node2.tileType == LEVEL_WALL) {
			  return true;
		  } else {
			  return false;
		  }
	}
	
	draw(color) {
		var x = this.col*tileSize;
		var y = this.row*tileSize;
		
		colorRect(x, y, tileSize, tileSize, color)
	}
}

/*
G: Score from start to this node. Increse by 10 for horizontal/vertical moves, increse 14 for diagonal (If included)
H: Score for remaining distanceance in straight line (Ignoring obstacles) Go diagonal until you hit row/column of target, then go straight
F: G + H. This is the score we look at to know which note to check next. Always check lowest. If equal, check lowest H
*/