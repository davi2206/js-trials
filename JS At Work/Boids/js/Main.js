const CANVAS_COLOR = 'blue'
const CANVAS_WIDTH = 1200;
const CANVAS_HEIGHT = 600;
const FPS = 250;
const FLOCK_SIZE = 10;
const BOID_SIZE = 5;
const BOID_COLOR = 'white';
const BOID_SPEED = 1;
const BOID_PERCEPTION = 50;
const BOID_STEER_FORCE = 1/2;
const BOID_STEER_TOLERANCE = (BOID_STEER_FORCE);

var scripts = ["Boid.js", "Flock.js", "ImageLoader.js", "VectorMath.js"];
var scriptsToLoad = 0;
var boid = null;
var flock = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, 'white', true, 'fishTank', 'fishCanvas')
	
	// Common Load Scripts
	loadScripts(scripts, "js/");
		
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = "Fish Tank?";
}

function start() {
	loadImages();
}

function runSim() {
	flock = new cFlock(FLOCK_SIZE);
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	flock.move();
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	//drawImgCentRot(cityImg, canvas.width/2, canvas.height/2, 0);
	
	flock.draw();
}