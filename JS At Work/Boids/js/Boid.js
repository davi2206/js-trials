function cBoid(x, y, ang, color = BOID_COLOR) {
	this.isMarker = false;
	this.steering = "none";
	
	this.speed = BOID_SPEED;
	this.perception = BOID_PERCEPTION + BOID_SIZE;
	this.r  = BOID_SIZE;
	this.x = x;
	this.y  = y;
	
	this.direction = ang;
	this.delta = 0;
	this.newDirection = 0;
	
	this.rad = getRadOfDeg(ang);
	
	this.nearby = [];
	
	this.move = function(snapshot) {
		this.steer(snapshot);
		
		this.rad = getRadOfDeg(this.direction)
		
		this.x += Math.cos(this.rad)*this.speed;
		this.y += Math.sin(this.rad)*this.speed;
		
		this.coord = this.wrapCoord(this.x, this.y);
		this.x = this.coord[0];
		this.y = this.coord[1];
	}
	
	this.steer = function(snapshot) {
		this.checkPeers(snapshot);
		this.align();
	}
		
	this.checkPeers = function(snapshot) {
		this.near = [];
		this.nearby = [];
		this.near = snapshot.filter(boid => (boid.x > this.x-BOID_PERCEPTION && boid.x < this.x+BOID_PERCEPTION));
		this.near = this.near.filter(boid => (boid.y > this.y-BOID_PERCEPTION && boid.y < this.y+BOID_PERCEPTION));
		this.near.forEach((boid) => this.addPeer(boid));
	}
	
	this.addPeer = function(boid) {
		this.xDif = (this.x - boid.x);
		this.yDif = (this.y - boid.y);
		this.cSq = Math.pow(this.xDif, 2) + Math.pow(this.yDif, 2);
		this.dist = Math.sqrt(this.cSq);
		
		if(this.dist < this.perception && this != boid) {
			this.nearby.push(boid);
		}
	}
	
	this.align = function() {
		this.delta = 0;
		this.ax = 0;
		this.ay = 0;
		this.nearby.forEach((boid) => this.addDelta(boid));
		
		this.steering = "none";
		if(this.delta < -0.5) {
			this.direction -= BOID_STEER_FORCE;
			this.steering = "left";
		}
		if (this.delta > 0.5) {
			this.direction += BOID_STEER_FORCE;
			this.steering = "right";
		}
		
		if(this.direction < 0) {
			this.direction += 360;
		}
		if (this.direction > 360) {
			this.direction -= 360;
		}
	}
	
	this.addDelta = function(boid) {
		this.angDelta = Math.abs(this.direction - boid.direction);
		
		if(this.angDelta < 180 && this.direction < boid.direction) {
			this.delta += 1; // Right
		} 
		if(this.angDelta < 180 && this.direction > boid.direction) {
			this.delta -= 1; // Left
		}
		
		if(this.angDelta > 180 && this.direction < boid.direction) {
			this.delta -= 1; // Left
		} 
		if(this.angDelta > 180 && this.direction > boid.direction) {
			this.delta += 1; // Right
		}
		
	}
	
	this.wrapCoord = function(x, y) {
		if(x > CANVAS_WIDTH) {
			x = 0;
		}
		if(y > CANVAS_HEIGHT) {
			y = 0;
		}
		if(x < 0) {
			x = CANVAS_WIDTH;
		}
		if(y < 0) {
			y = CANVAS_HEIGHT;
		}
		
		return [x,y];
	}
	
	this.draw = function() {
		switch(this.steering) {
			case "none":
				drawImgCentRot(fishStraight, this.x, this.y, this.rad);
				break;
			case "right":
				drawImgCentRot(fishRight, this.x, this.y, this.rad);
				break;
			case "left":
				drawImgCentRot(fishLeft, this.x, this.y, this.rad);
				break;
			default:
				colorCircle(this.x, this.y, this.r, color);
		}
		
		// FOR DEBUG ONLY
		// this.nx = Math.cos(this.rad) * BOID_PERCEPTION + this.x;
		// this.ny = Math.sin(this.rad) * BOID_PERCEPTION + this.y;
		// drawLineTo([this.x,this.y], [this.nx,this.ny]);
		// if(this.isMarker) {
			// colorCircle(this.x, this.y, this.perception, 'rgba(128, 0, 0, 0.3)');
		// }
	}
}