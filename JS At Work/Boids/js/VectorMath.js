/*
	FUNCTIONS FOR VECTORMATH
*/

function vector_add(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]+v2[0];
	vector[1] = v1[1]+v2[1];
	
	return vector;
}

function vector_sub(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]-v2[0];
	vector[1] = v1[1]-v2[1];
	
	return vector;
}

function vector_mult(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]*nr;
	vector[1] = v1[1]*nr;
	
	return vector;
}

function vector_div(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]/nr;
	vector[1] = v1[1]/nr;
	
	return vector;
}

/// DOES NOT WORK
function vector_avg(vectors) {
	var vector = [0,0];
	
	for(var v in vectors) {
		
	}
	console.log(vector);
		
	vector = vector_div(vector, vectors.length);
	
	return vector;
}

function vector_ang(v) {
	var ang = Math.atan(v[1]/v[0]);
	
	return ang;
}

function vector_ang_delta(v1, v2) {
	var ang = arctan(v[1]/v[0]);
	
	return ang;
}


/* TO IMPLEMENT
	Angle Delta, two Vectors
	Average Angle, multiple Vectors
*/