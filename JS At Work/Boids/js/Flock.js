
function cFlock(boids) {
	this.flock = []
	this.count = 0;
	
	while(this.count < boids) {
		this.deg = random_floor(0, 360);
		//this.deg = this.count;
		
		this.x = random_floor(0, CANVAS_WIDTH);
		this.y = random_floor(0, CANVAS_HEIGHT);
		
		this.flock.push(new cBoid(this.x, this.y, this.deg));
		this.count++;
	}
	
	// this.flock.push(new cBoid(CANVAS_WIDTH/2, CANVAS_HEIGHT/12*5, 170));
	// this.flock.push(new cBoid(CANVAS_WIDTH/2, CANVAS_HEIGHT/12*7, 190));
	// this.flock.push(new cBoid(CANVAS_WIDTH/2-100, CANVAS_HEIGHT/12*10-100, 270));
	// //this.flock.push(new cBoid(300, CANVAS_HEIGHT/5*2, 160));
	this.flock[0].isMarker = true;
	
	this.move = function() {
		this.snapshot = this.flock.slice();
		this.flock.forEach((boid) => boid.move(this.snapshot));
	}
	
	this.draw = function() {
		this.flock.forEach((boid) => boid.draw());
	}
}