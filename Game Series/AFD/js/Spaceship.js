// Class Player
class Spaceship {
	constructor() {
		this.x;
		this.y = canvas.height - FLY_ALTITUDE;
		
		this.score = 0;
		this.lives = PLAYER_LIVES;
		this.misile = undefined;
		
		this.nextX = 0;
		
		// Key press vars
		this.keyLeft = false;
		this.keyRight = false;
		this.keyFire = false;
		this.keyShield = false;
		
		this.right;
		this.left;
		this.fire;
		this.shield;
	}
	
	setupInput(rightKey, leftKey, fireKey, shieldKey) {
		this.right = rightKey;
		this.left = leftKey;
		this.fire = fireKey;
		this.shield = shieldKey;
	}

	reset() {
		this.score = 0;
		this.lives = PLAYER_LIVES;
		this.x = canvas.width/2;
		this.misile = undefined;
	} //End reset funct

	move() {
		this.nextX = this.x;
		this.nextY = this.y;
		
		if(this.keyShield) {
			// Activate shields
		}
		if(this.keyLeft) {
			this.nextX -= SHIP_SPEED;
		}
		if(this.keyRight) {
			this.nextX += SHIP_SPEED;
		}
		
		if(this.nextX > 0 && this.nextX < canvas.width) {
			this.x = this.nextX;
		}
		
		//// Move misile
		if(this.misile != undefined && this.misileMoving) {
			this.misileMoving = this.misile.move();
		} else {
			this.misile = undefined;
		}
	}
	
	fireMesile() {
		if(this.misile == undefined) {
			this.misile = new Misile(this.x);
			this.misile.move();
			this.misileMoving = true;
		}
	}
	
	draw() {
		drawImgCentRot(spaceshipImg, this.x, this.y, 0);
	}
}