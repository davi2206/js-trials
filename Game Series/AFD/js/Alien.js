// Class Alien
class Alien {
	constructor() {
		this.x = Math.floor(Math.random()*canvas.width);
		this.y = 0;
		
		this.nextX = 0;
		this.nextY = 0;
		this.speed = ALIEN_SPEED;
		
		this.hasHuman = false;
	}
	
	move() {
		this.nextX = this.x;
		this.nextY += this.speed;
		
		if(this.nextY < canvas.height+alienImg.height) {
			this.y = this.nextY;
		} else {
			this.grabHuman();
		}
		
		if(player.misile != undefined) {
			var mX = player.misile.x;
			var mY = player.misile.y;
			
			var xMin = this.x-alienImg.width/2;
			var xMax = this.x+alienImg.width/2;
			var yMin = this.y-alienImg.height/2;
			var yMax = this.y+alienImg.height/2;
			
			if(mX > xMin && mX < xMax && mY > yMin && mY < yMax) {
				this.die();
				player.misile = undefined;
			}
		}
		
		if(this.nextY < -5) {
			this.deliverHuman();
		}
	}
	
	die() {
		removeFromSet(this, aliens);
		liveAliens--;
	}
	
	grabHuman() {
		this.hasHuman = true;
		this.speed = -ALIEN_SPEED;
		this.y = canvas.height;
		liveHumans--;
	}
	
	deliverHuman() {
		this.hasHuman = false;
		this.speed = ALIEN_SPEED;
		this.y = 0;
	}
	
	fireMesile() {
		if(this.misile == undefined) {
			this.misile = new Misile(this.x);
			this.misile.move();
			this.misileMoving = true;
		}
	}
	
	draw() {
		if(this.hasHuman){
			drawImgCentRot(alienWHumanImg, this.x, this.y, 0);
		} else {
			drawImgCentRot(alienImg, this.x, this.y, 0);
		}
	}
}