// Class Shot
class Misile {
	constructor(x) {
		this.x = x;
		this.y = canvas.height - FLY_ALTITUDE;
		
		this.nextY = this.y;
	}
		
	reset() {
		this.score = 0;
		this.lives = PLAYER_LIVES;
		this.x = canvas.width/2;
	} //End reset funct

	move() {
		this.nextX = this.x;
		this.nextY = this.y - MISILE_SPEED;
		
		if(this.keyShield) {
			// Activate shields
		}
		if(this.keyLeft) {
			this.nextX -= SHIP_SPEED;
		}
		if(this.keyRight) {
			this.nextX += SHIP_SPEED;
		}
		
		if(this.nextY > 0) {
			this.y = this.nextY;
			return true;
		} else {
			return false;
		}
	}
	
	draw() {
		drawImgCentRot(misileImg, this.x, this.y, 0);
	}
}