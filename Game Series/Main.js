const DEBUG = true;

var tries = 3;
var level = 0;
var scriptsToLoad = 0;
var levelMap = new Map();

var aliensDescent = ["AFD/js/Inputs.js", "AFD/js/Misile.js", "AFD/js/Alien.js", "AFD/js/Spaceship.js", "AFD/js/ImageLoader.js", "AFD/js/Game.js"];
var brickSmash = ["BS/js/Inputs.js", "BS/js/Misile.js", "AFD/js/Alien.js", "AFD/js/Spaceship.js", "AFD/js/ImageLoader.js", "AFD/js/Game.js"];
var dungeonMaster = -1/* TBA */ //["BS/js/Inputs.js", "BS/js/Misile.js", "AFD/js/Alien.js", "AFD/js/Spaceship.js", "AFD/js/ImageLoader.js", "AFD/js/Game.js"];

window.onload = function() {
	levelMap.set(0, "AFD");
	levelMap.set(1, "BS");
	
	window.addEventListener("keydown", function(e) {
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	canvas = document.getElementById('gameCanvas');
	titleH1 = document.getElementById('gameTitle');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('If you see this, something broke!', canvas.width/10, canvas.height/3, 'blue', 80);
	
	playGames();
}

function playGames() {
	if(tries > 0){
		level = getLevel();
		
		switch(level) {
			case 0:
				loadGame(aliensDescent);
				titleH1.innerHTML = "Alien Forces Descent";
				break;
			case 1:
				loadGame(brickSmash);
				titleH1.innerHTML = "Brick Smash";
				break;
			default:
				titleH1.innerHTML = "Winner Winner Chicken Dinner!";
				colorRect(0, 0, canvas.width, canvas.height, "white");
				colorText('You Win!', canvas.width/2, canvas.height/2, 'green', 80);
				colorText('This is the end!', 100, 200, 'green', 80);
				colorText('Click the red X, top right,', 150, 300, 'green', 80);
				colorText('to go back to your life!', 161, 380, 'green', 80);
				break;
		}
	} else {
		colorRect(0, 0, canvas.width, canvas.height, "black");
		colorText('GAME OVER!', canvas.width/2, canvas.height/2, 'red', 80);
		titleH1.innerHTML = "You Lost";
	}
}

function loadGame(scripts) {
	scriptsToLoad = scripts.length;
	if(DEBUG) {
		for(var i = 0; i < scripts.length; i++) {
			var path = scripts[i];
			loadScript(path);
		}
	} else {
		for(var i = 0; i < scripts.length; i++) {
			var path = "https://theslowloris.com/wp-includes/js/games/"+scripts[i];
			loadScript(path);
		}
	}
}

function gameWon() {
	level++;
	document.cookie = "level="+levelMap.get(level);
	
	window.location.href = window.location.href;
}

function gameLost() {
	tries--;
	playGames();
}

function getLevel() {
	var queryString = window.location.search;
	var urlParams = new URLSearchParams(queryString);
	var sLevel = urlParams.get('level');
	
	var cookies = document.cookie.split(";");
	
	for(var i=0; i<cookies.length; i++) {
		if(cookies[i].includes("level=")){
			console.log(cookies[i].substring(cookies[i].indexOf("=")+1))
			sLevel = cookies[i].substring(cookies[i].indexOf("=")+1);
		}
	}
	
	if(sLevel == "" || sLevel == undefined) {
		sLevel = 0;
	}
	
	return sLevel;
}