var canvas;
var canvasContext;

const FPS = 20;
const LOCATE_MOUSE = false;

var startTime = 0;
var stopTime = 0;
var runTime = 0;
var steps = 0;

var searching = true;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('This is some sort of loading screen', canvas.width/10, canvas.height/3, 'blue', 80);
	
	startGame();
	startTime = performance.now();
	setInterval(function() {fpsLoop();}, 1000/FPS);
}

function startGame() {
	gameReset();
	registerEvents();
}

function gameReset() {
	initLevel();
}

function fpsLoop() {
	updateLives();
	drawLevel();
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

