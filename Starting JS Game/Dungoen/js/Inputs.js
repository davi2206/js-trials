// Mouse pos
var mouseX;
var mouseY;

// Map arrow keys
const KEY_LEFT_ARROW = 37;
const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;
const KEY_RESET = 82;

function registerEvents() {
	canvas.addEventListener('mousedown', handleMouseClick);
	canvas.addEventListener('mousemove', function(evt) {var mousePos = getMousePos(evt);});
	document.addEventListener('keydown', keyPressed);
	document.addEventListener('keyup', keyReleased);
	
	player.setupInput(KEY_UP_ARROW, KEY_RIGHT_ARROW, KEY_DOWN_ARROW, KEY_LEFT_ARROW, KEY_RESET);
}

// Mouse stuff
function handleMouseClick(evt) {
	if(showWinSrcn) {
		showWinSrcn = false;
		gameReset();
	}
}

function keyChange(keyEvt, player, value) {
	if(keyEvt.keyCode == player.up) {
		player.keyUp = value;
	}
	if(keyEvt.keyCode == player.down) {
		player.keyDown = value;
	}
	if(keyEvt.keyCode == player.left) {
		player.keyLeft = value;
	}
	if(keyEvt.keyCode == player.right) {
		player.keyRight = value;
	}
	if(keyEvt.keyCode == KEY_RESET) {
		gameReset();
	}
}

// Keyboard inputs
function keyPressed(evt) {
	keyChange(evt, player, true);
}

function keyReleased(evt) {
	keyChange(evt, player, false);
}