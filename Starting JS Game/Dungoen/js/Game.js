var canvas;
var canvasContext;

var showWinSrcn = false;

var player = new cPlayer();

const WALK_SPEED = 5;
const FPS = 120;
const LOCATE_MOUSE = false;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('Just waiting for images to load...', canvas.width/10, canvas.height/3, 'blue', 80);
	colorText('Sit back and relax', canvas.width/10*2, canvas.height/3*2, 'blue', 70);
	
	loadImages();
}

function startGame() {
	initLevel();
	gameReset();
	registerEvents();
	
	setInterval(function() {
		drawAll();
		player.move();
	}, 1000/FPS);
}

function gameReset() {
	initLevel();
	player.reset(playerPic, "Player");
}

function drawAll() {	
	// End Game screens
	if(showWinSrcn) {
		colorRect(0, 0, canvas.width, canvas.height, "black");
		colorText('You won!', canvas.width/10, canvas.height/3, 'green', 80);
		colorText('Click the mouse to continue...', canvas.width/10*2, canvas.height/3*2, 'blue', 70);
		return;
	}
	
	// Tracks
	drawLevel();
	
	// Car
	drawImgCentRot(player.playerPic, player.x, player.y, player.ang);
	
	colorText('Keys: ' + player.keys, canvas.width/10*8, canvas.height/10*9, 'gold', 30);
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		var mouseTrackX = Math.floor(mouseX / trackSize);
		var mouseTrackY = Math.floor(mouseY / trackSize);
		var mouseIndex = getArrayIndex(mouseTrackX, mouseTrackY);
		colorText(mouseTrackX+","+mouseTrackY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}