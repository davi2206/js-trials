var canvas;
var canvasContext;

var winner;

var paddleWidth = 10;
var paddleHeight = 100;
var paddleLeftX = 10;
var paddleRightX = 10;
var paddleLeftY = 200;
var paddleRightY = 200;

var ballWidth = 10;
var ballX = ballY = 0;
var ballSpeedX = 3;
var ballSpeedY = 1;
var deltaY = 0;
var deltaSpeed = 10;

var leftScore = 0;
var rightScore = 0;
var leftGames = 0;
var rightGames = 0;
var showWinSrcn = false;
const WIN_SCORE = 3;

const NET_HEIGHT = 20;
const NET_WIDTH = 2;
const FPS = 120;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	
	ballX = (canvas.width/2);
	ballY = (canvas.height/2);
	
	setInterval(function() {
		drawGame();
		moveAuto();
	}, 1000/FPS);
	
	canvas.addEventListener('mousedown', handleMouseClick);
	
	canvas.addEventListener('mousemove',
		function(evt) {
			var mousePos = calculateMousePos(evt);
			paddleLeftY = mousePos.y-paddleHeight/2;
		}
	);
}

function drawGame() {
	// Board
	canvasContext = canvas.getContext('2d');
	colorRect(0,0,canvas.width, canvas.height, 'black');
	
	if(showWinSrcn) {
		canvasContext.fillStyle = 'blue';
		canvasContext.font = 'normal 50px Arial';
		var winText = "Winner is " + winner;
		var textWidth = canvasContext.measureText(winText).width;
		canvasContext.fillText(winText, canvas.width/2-textWidth/2, canvas.height/3);
		
		var continueText = "Click to play again";
		var textWidth = canvasContext.measureText(continueText).width;
		canvasContext.fillText(continueText, canvas.width/2-textWidth/2, canvas.height/3*2);
		
		return;
	}
	
	// Draw net
	for(var i = 0; i < canvas.height; i += NET_HEIGHT*2) {
		colorRect(canvas.width/2-NET_WIDTH/2, i, NET_WIDTH, NET_HEIGHT, 'white');
	}
	
	// Game paddles
	paddleLeftX = 10;
	paddleRightX = (canvas.width-paddleWidth-10);
	colorRect(paddleLeftX, paddleLeftY, paddleWidth, paddleHeight, 'white');
	colorRect(paddleRightX, paddleRightY, paddleWidth, paddleHeight, 'white');
	
	// Ball
	colorCircle(ballX, ballY, ballWidth, 'green');
	
	// Score
	canvasContext.font = 'normal 100px Arial';
	canvasContext.fillStyle = "blue";
	
	var textWidthL = canvasContext.measureText(leftScore).width;
	var textWidthR = canvasContext.measureText(rightScore).width;
	
	canvasContext.fillText(leftScore, canvas.width/3-textWidthL/2, canvas.height/3);
	canvasContext.fillText(rightScore, canvas.width/3*2-textWidthR/2, canvas.height/3);
}

function moveAuto(){
	if(showWinSrcn) return;
	
	moveBall();
	movePaddle();
}

function moveBall(){
	// Horizontal movement
	if(ballX > (paddleRightX-ballWidth)) {
		if(((ballY+ballWidth) < paddleRightY) || ((ballY-ballWidth) > paddleRightY+paddleHeight)) {
			leftScore++;
			ballReset();
		} else {
			ballSpeedX = -ballSpeedX;			
			deltaY = ballY - (paddleRightY+paddleHeight/2);
			ballSpeedY = deltaY / deltaSpeed;
		}
	}
	if(ballX < (paddleLeftX+paddleWidth+ballWidth)){
		if(((ballY+ballWidth) < paddleLeftY) || ((ballY-ballWidth) > paddleLeftY+paddleHeight)) {
			rightScore++;
			ballReset();
		} else {
			ballSpeedX = -ballSpeedX;
			deltaY = ballY - (paddleLeftY+paddleHeight/2);
			ballSpeedY = deltaY / deltaSpeed;
		}
	}
	
	// Vertical movement
	if(ballY > (canvas.height-ballWidth)) {
		ballSpeedY = -ballSpeedY;
	}
	if(ballY < (ballWidth)){
		ballSpeedY = -ballSpeedY;
	}
	
	ballX += ballSpeedX;
	ballY += ballSpeedY;
}

function movePaddle(){
	var paddleCenter = (paddleRightY+paddleHeight/2)
	
	var paddleRightRandMove = (leftScore+1)*2;//(Math.random() * 10) + 1;
	
	if(ballY < paddleCenter-(paddleHeight/4)) {
		paddleRightY -= paddleRightRandMove;
	}
	else if(ballY > paddleCenter+(paddleHeight/4)) {
		paddleRightY += paddleRightRandMove;
	}
}

function colorRect(x, y, width, height, color){
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function colorCircle(x, y, r, color){
	canvasContext.fillStyle = color;
	canvasContext.beginPath();
	canvasContext.arc(x, y, r, 0, Math.PI*2, true);
	canvasContext.fill();
}

function calculateMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	var mouseX = evt.clientX - rect.left - root.scrollLeft;
	var mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function handleMouseClick(evt) {
	if(showWinSrcn) {
		leftScore = rightScore = 0;
		showWinSrcn = false;
	}
}

function ballReset() {
	// Check for wins
	if(leftScore >= WIN_SCORE) {
		leftGames++;
		showWinSrcn = true;
		winner = "Human"
	}
	if(rightScore >= WIN_SCORE) {
		rightGames++;
		showWinSrcn = true;
		winner = "Computer"
	}
	
	ballX = canvas.width/2;
	ballY = canvas.height/2;
	ballSpeedX = -ballSpeedX;
	ballSpeedY = 0;
}



































