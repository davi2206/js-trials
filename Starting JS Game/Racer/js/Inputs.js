

// Mouse pos
var mouseX;
var mouseY;

// Map arrow keys
const KEY_LEFT_ARROW = 37;
const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;

const KEY_W = 87;
const KEY_A = 65;
const KEY_S = 83;
const KEY_D = 68;

function registerEvents() {
	canvas.addEventListener('mousedown', handleMouseClick);
	canvas.addEventListener('mousemove', function(evt) {var mousePos = getMousePos(evt);});
	document.addEventListener('keydown', keyPressed);
	document.addEventListener('keyup', keyReleased);
	
	blueCar.setupInput(KEY_UP_ARROW, KEY_RIGHT_ARROW, KEY_DOWN_ARROW, KEY_LEFT_ARROW);
	greenCar.setupInput(KEY_W, KEY_D, KEY_S, KEY_A);
}

// Mouse stuff
function handleMouseClick(evt) {
	if(showWinSrcn || showLoseSrcn) {
		showWinSrcn = false;
		showLoseSrcn = false;
		gameReset();
	}
}

function keyChange(keyEvt, carInstance, value) {
	if(keyEvt.keyCode == carInstance.up) {
		carInstance.keyGas = value;
	}
	if(keyEvt.keyCode == carInstance.down) {
		carInstance.keyReverse = value;
	}
	if(keyEvt.keyCode == carInstance.left) {
		carInstance.keyLeft = value;
	}
	if(keyEvt.keyCode == carInstance.right) {
		carInstance.keyRight = value;
	}
}

// Keyboard inputs
function keyPressed(evt) {
	keyChange(evt, blueCar, true);
	keyChange(evt, greenCar, true);
}

function keyReleased(evt) {
	keyChange(evt, blueCar, false);
	keyChange(evt, greenCar, false);
}