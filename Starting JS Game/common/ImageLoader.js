// Create elements for images
var playerPic = document.createElement("img");

var levelPics = [];

var wallPic = document.createElement("img");
var roadPic = document.createElement("img");
var goalPic = document.createElement("img");

var picsToLoad = 0;

function loadImages(imageMap) {
	picsToLoad = imageMap.length;
	
	for (var [k, v] of imageMap.entries()) {
		k = document.createElement("img");
		beginLoadImg(k, v);
	}
}

function beginLoadImg(imgElement, fileName) {
	imgElement.onload = function() {
		picsToLoad--;	
		if(picsToLoad == 0) {
			startGame();
		}
	}
	imgElement.src = fileName;
}