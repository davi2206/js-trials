function loadCanvas(width, height, color = 'white', showError = true, parentCanvasName = 'parentCanvas', canvasName = 'gameCanvas') {
	parentCanvas = document.getElementById(parentCanvasName);
	canvas = document.getElementById(canvasName);
	canvasContext = canvas.getContext('2d');
	
	parentCanvas.width = width;
	parentCanvas.height = height;
	canvas.width = width;
	canvas.height = height;
	
	colorRect(0, 0, canvas.width, canvas.height, color);
	if(showError) {
		colorText('If you see this, something broke!', canvas.width/10, canvas.height/3, 'red', 80);
	}
}

function drawLineTo(start, end, stroke = 'black', width = 5) {
	if (stroke) {
        canvasContext.strokeStyle = stroke;
    }
	
	canvasContext.beginPath();
    canvasContext.moveTo(...start);
    canvasContext.lineTo(...end);
    canvasContext.stroke();
}

function drawLineDirection(start, angDeg, length, stroke = 'black', width = 5) {
	if (stroke) {
        canvasContext.strokeStyle = stroke;
    }
	
	var angRad = getRadOfDeg(angDeg);
	
	var x = Math.cos(angRad)*length + start[0];
	var y = Math.sin(angRad)*length + start[1];
	
	canvasContext.beginPath();
    canvasContext.moveTo(...start);
    canvasContext.lineTo(x, y);
    canvasContext.stroke();
	
	// DEBUG: 
	colorCircle(start[0], start[1], 5, 'black');
}

function colorRect(x, y, width, height, color){
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function colorRect(x, y, width, height, fillColor, strokeColor){
	canvasContext.fillStyle = fillColor;
	canvasContext.strokeStyle = strokeColor;
	//canvasContext.fillRect(x, y, width, height);
	
	canvasContext.beginPath();
	canvasContext.rect(x, y, width, height);
	canvasContext.stroke();
	canvasContext.fill();
}

function colorCircle(x, y, r, color){
	canvasContext.fillStyle = color;
	canvasContext.beginPath();
	canvasContext.arc(x, y, r, 0, Math.PI*2, true);
	canvasContext.fill();
}

function colorText(words, posX, posY, color, size) {
	canvasContext.font = 'normal '+size+'px Arial';
	canvasContext.fillStyle = color;
	canvasContext.fillText(words, posX, posY);
}

function drawImgCentRot(img, x, y, ang) {
	canvasContext.save();
	canvasContext.translate(x, y);
	canvasContext.rotate(ang);
	canvasContext.drawImage(img, -img.width/2, -img.height/2);
	canvasContext.restore();
}

function getMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouseX = evt.clientX - rect.left - root.scrollLeft;
	mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function removeFromSet(object, set) {
	for(var i = set.length-1; i>=0; i--) {
		if(set[i] == object) {
			set.splice(i, 1);
		}
	}
}

// Loop over and load scripts
function loadScripts(scripts, basePath) {
	scriptsToLoad = scripts.length;
	
	for(var i = 0; i < scripts.length; i++) {
		var path = basePath+scripts[i];
		loadScript(path);
	}
}

// Load External JS
function loadScript(url){
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function callback() {
	scriptsToLoad--;
	
	if(scriptsToLoad == 0) {
		start();
	}
}

function getRadOfDeg(deg) {
	while(deg > 360) {
		deg -= 360;
	}
	while(deg < 0) {
		deg += 360;
	}
	return (Math.PI * deg / 180)
}

function random(min, max) {
	return (Math.random() * (max - min) + min);
}

function random_floor(min, max) {
	return Math.floor(random(min, max));
}